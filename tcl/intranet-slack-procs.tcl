ad_library {

     Slack integration package
         
    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @creation-date 2018-02-02
}

package require TclCurl

namespace eval slack {}

ad_proc slack::post {
    {-webhook_url ""}
    {-channel ""}
    {-username ""}
    {-icon ""}
    {-text ""}
} {
    Post a new slack message to the webhook_url using https post
    
    @param webhook_url URL for the webhook
    @param channel Channel into which to post to
    @param username Name of the user used for posting
    @param icon Icon to use for the postin
    
    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
} {
    
    if {$webhook_url eq ""} {
        set webhook_url [parameter::get_from_package_key -package_key "intranet-slack" -parameter SlackWebhookURL]
    }
    if {$webhook_url eq ""} {
	ns_log Debug "Missing Slack integration"
	return
    } else {
	if {$username eq ""} {
	    set username "[ns_info server]"
	}
	if {$channel eq ""} {
	    set channel "#general"
	}
	set payload "\{\"channel\": \"$channel\", \"icon_emoji\": \"$icon\",
        \"username\": \"$username\",\"text\": \"[im_quotejson $text]\"
        \}"
        
	curl::transfer -url $webhook_url -post 1 -httppost [list name payload contents $payload]
    }
}

ad_proc slack::post_with_attachment {
    {-webhook_url ""}
    {-channel ""}
    {-username ""}
    {-icon ""}
    -fallback
    {-pretext ""}
    {-color "#D00000"}
    {-value ""}
} {
    Post a new slack message to the webhook_url using https post

    @param webhook_url URL for the webhook
    @param channel Channel into which to post to
    @param username Name of the user used for posting
    @param icon Icon to use for the postin

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
} {

    if {$webhook_url eq ""} {
        set webhook_url [parameter::get_from_package_key -package_key "intranet-slack" -parameter SlackWebhookURL]
    }
    if {$webhook_url eq ""} {
	ns_log Debug "Missing Slack integration"
	return
    } else {
	if {$username eq ""} {
	    set username "[ns_info server]"
	}
	if {$channel eq ""} {
	    set channel "#general"
	}
	set payload "\{\"channel\": \"$channel\", \"icon_emoji\": \"$icon\",
        \"username\": \"$username\",
        \"attachments\": \[ \{
                \"fallback\":\"[im_quotejson $fallback]\",
                \"pretext\": \"[im_quotejson $pretext]\",
                \"color\":\"$color\",
                \"fields\":\[\{
                    \"value\":\"[im_quotejson $value]\",
                    \"short\":\"false\"
                \}\]
            \}\]
        \}"
	
	curl::transfer -url $webhook_url -post 1 -httppost [list name payload contents $payload]
    }
}

ad_proc slack::post_wd_errors {
} {
    Tcl version of packages/monitoring/bin/aolserver-errors.pl, to run
    inside of AOLserver rather than forking a Perl script.
    <p>
    Saves its run-to-run state in $lastreadfile, so AOLserver must have
    write permission to the directory where $log_file is located.

    @author David Walker (openacs@grax.com)
    @author Andrew Piskorski (atp@piskorski.com)
} {

    set log_file [ns_info log]
    set lastreadfile "${log_file}.slack.lastread"

    if {[file exists $lastreadfile]} {
        source "${lastreadfile}"

        # That will set the lastread (which is bytes) and lastread_time
        # (which is a string like 'Tue Apr 09 18:44:49 2002') variables.
    }
    set log_file_size [file size $log_file]

    if { ![info exists lastread] || $lastread > $log_file_size } {
        set lastread 0
        set lastread_time "First Run"

        # If the log has been rolled, presumably it will now be smaller
        # in size than it was last time.  So in that case, always read
        # from the beginning of the file.
        #
        # TODO: But, it isn't GUARANTEED that it will always be smaller.
        # Is there any other way for us to detect if the log has been
        # rolled or otherwise fooled with?  Save the first line from the
        # log into our $lastreadfile, if the first line in the current
        # log is different, then we know the log has been rolled.
        # --atp@piskorski.com, 2002/04/09 13:13 EDT
    }

    set fh [open $log_file]
    seek $fh $lastread

    set in_error_p 0
    set in_following_notice_p 0
    set num_errors 0

    # Note that in the while loop below, if we simply read till EOF we
    # will have a race condition with the other threads writing to the
    # log - bad!  A stern chase is a long chase, so if the log is
    # growing like crazy because our server is, say, looping over and
    # over on an error (yes, I've seen it happen...), by the time this
    # thread catches up, we may easily end up trying to email 50+ MB of
    # error messages.  We avoid this race condition by checking our
    # position in the file with tell:
    #
    # --atp@piskorski.com, 2002/07/26 00:46 EDT


    # Read in all the lines
    set access_log [ns_config ns/server/[ns_info server]/module/nslog file]
    set slack_icon [parameter::get_from_package_key -package_key "intranet-slack" -parameter ErrorIcon]
    set slack_channel [parameter::get_from_package_key -package_key "intranet-slack" -parameter ErrorChannel]
    set username [parameter::get_from_package_key -package_key "intranet-slack" -parameter ErrorUser]
    set counter 0
    while { ![eof $fh] && ([tell $fh] <= $log_file_size) } {
        gets $fh thisline

        # Each line in the AOLserver error (server) log looks like this:
        # [13/Jul/2002:00:43:29][2074.5][-sched-] Notice: Running scheduled proc wd_mail_errors...

        # Using non-greedy like this works:
        #   {^\[(.*?)\]\[(.*?)\]\[(.*?)\](.*)$}
        # but let's use negated character classes instead:

        if { [regexp -expanded -- {
            ^\[([^\]]*)\]
            \[([^\]]*)\]
            \[([^\]]*)\]
            (.*)$
        } $thisline xxx var1 var2 var3 var4] } {
            set time $var1
            set origin_list [split [string trim $var3 "-"] ":"]
            set message [string trim $var4]

            if {[string first "Error:" $message] == 0} {
                set in_error_p 1
                set in_following_notice_p 0
                incr num_errors
                set posttext ""
                switch [lindex $origin_list 0] {
                    sched {
                        set origin "Scheduled"
                    }
                    default {
                        # Filter the lines
                        set url_list [list]

                        if {![catch {exec grep $time $access_log} results]} {
                            set matches [split $results "\n"]
                            foreach access $matches {
                                set elements [split $access]
                                lappend url_list [lindex $elements 6]
                            }
                        }
                        set url_counter 0
                        foreach url $url_list {
                            incr url_counter
                            append posttext "${url_counter}.<[ad_url]$url|$url>\n"
                        }
                    }
                }
                incr counter
                set error_time($counter) $time
                set error_message($counter) $message
                set error_posttext($counter) $posttext
            } elseif {[string first "Notice:" $message] == 0} {
                if { $in_error_p } {
                    set in_following_notice_p 1
                } else {
                    set in_following_notice_p 0
                }
                set in_error_p 0
            } else {
                set in_error_p 0
                set in_following_notice_p 0
            }
        } else {
            # The regexp didn't match, so whatever this line is, it's NOT
            # the start of a normal AOLserver ns_log message.
            if { $in_error_p } {
                if {![string match "*HINWEIS*" $thisline ]} {
                    append error_posttext($counter) "\n$thisline"
                }
            }
        }
    }

    for {set x 1} {$x<=$counter} {incr x} {
	set fallback "$error_time($x): $error_message($x)"
	set pretext  "*$error_time($x)*: $error_message($x)"
	set value  "$error_posttext($x)"
        slack::post_with_attachment -channel $slack_channel -icon $slack_icon -username $username \
            -fallback $fallback \
            -pretext $pretext \
            -color "#D00000" \
            -value $value
    }

    set file_read_size [tell $fh]
    close $fh

    set fh [open $lastreadfile w]
    puts $fh "set lastread \"${file_read_size}\""
    puts $fh "set lastread_time \"[ns_fmttime [clock seconds]]\""
    close $fh
}

